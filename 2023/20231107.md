---
title: Meeting Minutes 2023-11-07
date: 2023-10-07
attendees:
  - lancealbertson,oregonstateuniversity
  - maximillianschmidt,oregonstateuniversity
  - stangowen,ibm
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * Active
     * Potential: OSU Researcher about Adversarial Attacks
       * Original request came through OSL ticketing, but we directed them to the HUB.
   * Ongoing
     * [#198019: Request by Portable SIMD](https://requests.openpower.foundation/Ticket/Display.html?id=198019)
       * Max will send an update to see how they are doing
     * [#198230: Request by 9front](https://requests.openpower.foundation/Ticket/Display.html?id=198230)
       * Paused - requestor still developing/getting started on their own machine
       * Requested system is powered down
   * Resolved
2. OSU OpenShift
   * Update OPF site
     * Remove current "advertisements" about OpenShift/container availability
     * Split out VM with or without GPU (default VM without GPU)
     * With physical worker nodes out of commission (for OpenShift), we technically have 3 more bare metal checkout machines
       * Pre-install Ubuntu 22.04 and AlmaLinux 9 on two nodes
       * Possibly look into adding third node as OpenStack with PCI-passthru for FPGA.
3. Other Items
   * Max and/or Tosh (TODO): Add SLES Image(s)
     * No Updates
   * POWER10 Status at OSU
     * Waiting on a meeting for setup instructions
   * Holiday Meeting Schedule
     * Cancel 11/21/2023 Meeting due to US Thanksgiving week
     * Cancel 12/19/2023 Meeting due to Holidays

## Next meeting

{{< localdatetime date="2023-12-05" time="17:30" >}}
