---
title: Meeting Minutes 2023-01-17
date: 2023-01-17
attendees:
  - brunomesnet,ibm
  - lancealbertson,oregonstateuniversity
  - stangowen,ibm
  - toshaanbharvani,vantosh

draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. New Requests
   * [#1527: Request by libgcrypt for OpenPOWER HUB access through OSU Open Source Lab
](https://requests.openpower.foundation/Ticket/Display.html?id=1527)
     * Will reply today or tomorrow now that OPF email is working properly again
   * Also reply to older requests
2. OSU OpenShift
   * Max is on vacation for the next two weeks, so no updates
3. OFP Form and RT Integration
   * RT Integration
     * Permission issues with web user on OSUOSL RT to HUB queue
     * TODO: Lance make adjustments that Tosh has requested
   * OPF Form
     * POWER8 has been removed
     * TODO: Additional minor adjustments (OS versions, etc)
4. Meeting Calendar Invitiations
   * New OPF [calendar site](https://calendar.openpower.foundation/)
   * Invitations sent using new system
   * ~~Cancellation of old meetings will be done this week~~
   * TODO: Look into why new invitations aren't going out

## Next meeting

{{< localdatetime date="2023-01-24" time="22:00" >}}
