---
title: Meeting Minutes 2023-10-17
date: 2023-10-17
attendees:
  - lancealbertson,oregonstateuniversity
  - maximillianschmidt,oregonstateuniversity
  - stangowen,ibm
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * Active
     * [#198230: Request by 9front](https://requests.openpower.foundation/Ticket/Display.html?id=198230)
       * User is working more with local resources - checked out machine now offline but still "reserved"
     * [Kata Containers](https://katacontainers.io/)
         * May see a request come through for bare-metal access
         * None so far...
   * Ongoing
     * [#198019: Request by Portable SIMD](https://requests.openpower.foundation/Ticket/Display.html?id=198019)
       * No updates since last meeting
   * Resolved
2. OSU OpenShift
   * Remove current "advertisements" about availability/capability
3. RT Ticket Queue workflow
   * Tosh: To create a common "OSL student account" - osuosl-student <rootmail-students@osuosl.org>
     * Local account created, need to do a password reset.
4. Other Items
   * Max (TODO): Add SLES Image(s)
     * No Update: Going with trial license --> ISO
   * Tosh (TODO): Add SLES Image(s)
     * Has started looking
   * POWER10 Status at OSU
     * Waiting on a meeting for setup instructions
   * Push back this (US/EU) meeting time?
     * Pushed back to 9:30AM PDT (17:30 UTC)

## Next meeting

{{< localdatetime date="2023-11-07" time="17:30" >}}
