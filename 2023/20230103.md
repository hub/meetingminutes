---
title: Meeting Minutes 2023-01-03
date: 2023-01-03
attendees:
    - alexandrecastellane,ibm
    - stangowen,ibm
    - brunomesnet,ibm
    - fabricemoyen,ibm
    - jameskulina,openpowerfoundation
    - lancealbertson,oregonstateuniversity
    - maximillianschmidt,oregonstateuniversity
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. **OSU OpenShift**
   * Still working on rebuilding
   * Getting new student(s) going on this as they return

2. **OFP Form and RT Integration (TOP PRIORITY)**
   * TODO: Waiting on Tosh to implement their backend and test access
     * Tosh has staged everything and will test everything tomorrow

3. **Baremetal access for members**
   * TODO: Tosh will talk with Todd
     * No update

4. **GPU VM Images**
   * Refactored how base images are built
   * TODO: Add helper bash profile to set PATH to GPU binaries
   * TODO: Update/Release images

## Next meeting

{{< localdatetime date="2023-01-10" time="22:00" >}}
