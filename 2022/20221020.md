---
title: Meeting Minutes 2022-10-20
date: 2022-10-20
attendees:
  - bobszabo,ibm
  - alexandrecastellane,ibm
  - lancealbertson,oregonstateuniversity
  - chrissullivan,oregonstateuniversity
  - maximillianschmidt,oregonstateuniversity
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

### Agenda points

1. **AC922 Testing Status**
   * The machines are ready for testing once the OPF Portal is integrated with OSU environment.
   * Lance will build a new set of images and and publish them. Will deal with any issues later if they are any.

2. **OpenShift**
   * Max has started on the POWER install.
     * Still going with the "single all-in-one node".
   * Max has access to Fabrice's cluster for comparison, if needed.
     * **Fabrice out with injury. Possibly out for 10+ days.**
   * Network diagram is available in NextCloud for review. <https://files.openpower.foundation/f/6806>
   * Domain name possibility `openshift.opfhub.osuosl.org`
     * Need to figure out internal vs external domain for which the cluster is "listening" on or responding to.
   * If OpenStack becomes too much of a barrier to completion of the OpenShift cluster, we will fall back to installing on KVM only.

3. **OFP Form and RT Integration - HIGH PRIORITY**
   * The OPF portal is still not ready to be integrated with OSU.
   * Lance has everything ready on the OSL end.
     * Both Lance and Max nudged Toshaan.
   * Tooshan will be working on the OPF end, and assisting with the OSL end.
     * Tooshan can send an email to `support@osuosl.org` to auto-generate an account for Lance to elevate.
       * Still waiting on Toshaan to send email with details on what we need to do on our end
   * Testing to start next week.

4. **OPF HUB SIG WG**
   * Looks like 8AM PDT is still the best time for most people, at least for the initial meeting.
     * Long term it will need to change due to scheduling conflicts with LibreBMC.
   * Target for the first call is rolled to 2022-10-27.
     * Lance to send out a reminder.

5. **Super Computing '22**
   * SC22 is still Nov 13-18, 2022 @ <https://sc22.supercomputing.org/>
   * It is desired that the OPF HUB SIG WG drive our presence and content for this conference.
   * OPF has opportunity to be at the OpenPOWER booth.
     * Working with James on getting expo presenter tickets.
       * Max to poke James on any updates.
     * Max and/or Chris can present or demo the HUB.
       * Need dedicated presentation/demo device (laptop), which may need special access (MAC address).
       * Need some large screen to present.
     * Option needed to make the demo easily accessible afterwards (video, blog, etc.)
   * **This target also emphasizes the importance of completing the OPF Portal integration with OSU as soon as practical, as we will need time for testing, andetc.. Toshaan will need to work on this soon.**
   * Some sort of annoucement from OPF regarding new HUB resource?
     * At least a blog post and/or annoucement in Slack channels.

## Next meeting

{{< localdatetime date="2022-10-27" time="15:00" >}}
