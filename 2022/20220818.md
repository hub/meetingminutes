---
title: Meeting Minutes 2022-08-18
date: 2022-08-18
attendees:
  - bobszabo,ibm
  - toshaanbharvani,vantosh
  - maximillianschmidt,oregonstateuniversity
  - lancealbertson,oregonstateuniversity
  - kenlett,oregonstateuniversity
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

### Agenda points

1. **OSU/OPF webform integration - TOP PRIORITY**
    * Tosh still working on editing the HUB web form and integration to OPF RT.
        * Changing layout to filter resources, therefore requests, to specific HUBs with the resources available.
    * Lance and Max scheduling time to look at OSL's RT this next week.
        * OPF on 5.0.2 - OSL on 4.0.4.
        * OSL moving off of legacy system.
    * We've also had three requests for access to HUB resources already.
    * James will be working on the marketing of the HUB resoures.
2. **AC922s Are Basically Ready**
    * Need to find more testers.
        * Max has tested the GPUs via PCI-passthrough. Driver installed and compiling [relion](https://github.com/3dem/relion) with CUDA.
    * Need to work on OpenStack images still.
        * Lance and Max to start looking at adding automation to the build process.
    * Need to look at load balancing since all OpenStack instances are currently landing on one machine.
    * Need to look at fan speeds being inconsistent across machines.
3. **OpenShift Status**
    * Still waiting on OSU Foundation to accept "gift" from Red Hat.
        * Once Chris is back from vacation, we will hopefully get an update.
    * Max working on getting the foundation set for the cluster with the IC922 being used for the control plane.
        * Getting RHCOS image available for OpenStack instances
        * Building OpenShift specific flavor for Master Nodes

## Next meeting

{{< localdatetime date="2022-08-25" time="15:00" >}}
