---
title: Meeting Minutes 2024-03-19
date: 2024-03-19
attendees:
  - lancealbertson,oregonstateuniversity
  - stangowen,ibm
  - toshannbharvani,vantosh
  - jameskulina,openpowerfoundation
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * Active
     - [#200640: Request by power-devops.com for OpenPOWER HUB access through VanTosh OpenPOWER HUB](https://requests.openpower.foundation/Ticket/Display.html?id=200640)
       - Tosh will take care of this
   * Ongoing
   * Resolved/Closed
2. OSU HUB grant
   * Status of renewal
     * James needs to contact Chris with options
3. Other Items
   * OSS-NA Session Mini Hardware Summit discussion (April 15, 2024 Seattle)
     * Lance needs to still send a bio
     * Tosh:
       * Will this be recorded? -- no
       * Max and Lance should have a blog post about their sessions

## Next meeting

{{< localdatetime date="2024-04-02" time="16:30" >}}
