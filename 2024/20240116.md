---
title: Meeting Minutes 2024-01-16
date: 2024-01-16
attendees:
  - lancealbertson,oregonstateuniversity
  - stangowen,ibm
  - toshaanbharvani,vantosh
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * Active
     * None
   * Ongoing
     * [#198019: Request by Portable SIMD](https://requests.openpower.foundation/Ticket/Display.html?id=198019)
       * Awaiting feedback
     * [#198230: Request by 9front](https://requests.openpower.foundation/Ticket/Display.html?id=198230)
       * Max: Pinged on Dec 13, 2023, no reply
2. Update OPF site
   * [Open Pull Request awaiting reviews](https://git.openpower.foundation/website/openpower.foundation/pulls/58)
     * Remove current "advertisements" about container availability
     * VM and Bare Metal can leverage GPUs or FPGAs
3. Bare Metal Nodes
   * With physical worker nodes out of commission (for OpenShift), we technically have 3 more bare metal checkout machines
     * Pre-install Ubuntu 22.04 and AlmaLinux 9 on two nodes
     * Possibly look into adding third node as OpenStack with PCI-passthru for FPGA.
       * No updates
4. Other Items
   * Openstack OS Upgrades
     * All OPH nodes are now running AlmaLinux 8

## Next meeting

{{< localdatetime date="2024-02-06" time="17:30" >}}
