---
title: Meeting Minutes 2024-08-20
date: 2024-09-03
attendees:
  - lancealbertson,oregonstateuniversity
  - stangowen,ibm
  - jameskulina,linuxfoundation
  - stevenmunroe,individual
  - toshaan,vantosh

draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * New
     - [#201126: Request by Selkies-GStreamer for OpenPOWER HUB access through Oregon State University Open Source Lab](https://requests.openpower.foundation/Ticket/Display.html?id=201126)
       - Asking about GPU, but seeing if VMs also work
       - Pinged them today
   * Active
   * Ongoing
     - [#200816: Request by AMD ROCm 9 for OpenPOWER HUB access through Oregon State University Open Source Lab](https://requests.openpower.foundation/Ticket/Display.html?id=200816)
       - No new updates
       - Moving to P10 box hosted by OSL for GPU work
   * Resolved/Closed

2. Other Items
   * OSU Service Contract Status
     - New service contract sent to OSU Procument for review
       - Having trouble getting this converted over to a service contract. Pinged them again today.
     - Waiting on final review before being sent out for signatures

## Next meeting

{{< localdatetime date="2024-09-03" time="19:30" >}}
