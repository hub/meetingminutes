---
title: Meeting Minutes 2024-03-05
date: 2024-03-05
attendees:
  - lancealbertson,oregonstateuniversity
  - maximillianschmidt,oregonstateuniversity
  - stangowen,ibm
  - jameskulina,openpowerfoundation
draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * Active
   * Ongoing
   * Resolved/Closed
     * [#200523: Request by format-ripper](https://requests.openpower.foundation/Ticket/Display.html?id=200523)
       * Max closed
2. Bare Metal Nodes
   * With physical worker nodes out of commission (for OpenShift), we technically have 3 more bare metal checkout machines
     * Leave for now, if OKD becomes an option
   * Max checked out oph9 for Open-CE building (main builder being checked for hardware issues)
3. OSU HUB grant
   * Grant that employed Max is now complete
   * Status of renewal - TBD in Board Meeting
4. Other Items
   * US Daily Saving time changes March 10
     * March 19 meeting will happen at 2:30PM PDT
   * OSS-NA Session Mini Hardware Summit discussion (April 15, 2024 Seattle)
     * Chris, Lance, and Max submitted titles, abstracts, and bios to James for presentations
   * RDO + OKD - CentOS willing to work on things found in both
     * Biweekly open hour to discuss or leverage help
     * [CentOS CONNECT OKD Session](https://www.youtube.com/watch?v=LocoEPmbL4U)
   * Option to auto-vet incoming VM requests
     * James: IBM Z folks might have an option for us
     * Likely running OpenStack?

## Next meeting

{{< localdatetime date="2024-03-19" time="21:00" >}}
