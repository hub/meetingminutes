u---
title: Meeting Minutes 2024-10-01
date: 2024-10-01
attendees:
  - lancealbertson,oregonstateuniversity
  - toshaan,vantosh
  - stangowen,ibm

draft: false
---

## Call to order

### Anti-trust reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines. Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines. Copies of the Antitrust Guidelines are available at : [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

## Agenda

1. OPF HUB Requests
   * New
     - [#201483: Request by Fisheries Conflict Project for OpenPOWER HUB access through Oregon State University Open Source Lab](https://requests.openpower.foundation/Ticket/Display.html?id=201483)
       - Asked if a VM with GPU will work
           - Said yes, will try and get this deployed today
           - Ask them which models are they using? Do they need help with the models?
     - [#201126: Request by Selkies-GStreamer for OpenPOWER HUB access through Oregon State University Open Source Lab](https://requests.openpower.foundation/Ticket/Display.html?id=201126)
       - Pinged today
   * Active
   * Ongoing
     - [#200816: Request by AMD ROCm 9 for OpenPOWER HUB access through Oregon State University Open Source Lab](https://requests.openpower.foundation/Ticket/Display.html?id=200816)
       - Working on budget to purchase hardware for this
       - Ask James to see if AMD can help with loaning a GPU or ask for a discount
   * Resolved/Closed

2. Other Items
   *
## Next meeting

{{< localdatetime date="2024-10-22" time="21:00" >}}
